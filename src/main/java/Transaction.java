public class Transaction {
    private int id;
    private TransactionType type;
    private double value;

    public Transaction(int id, TransactionType type, double value) {
        this.id = id;
        this.type = type;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public TransactionType getType() {
        return type;
    }

    public double getValue() {
        return value;
    }
}
