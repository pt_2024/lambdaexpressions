@FunctionalInterface
interface Adder {
    double add(double n1, double n2);
}
