import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;


class Main
{
    private static void functionalInterfaceExample() {
        FuncInterface fobj = (x)->System.out.println(2*x);
        FuncInterface fobj2 = (x)->System.out.println(3*x);

        fobj.abstractFun(5);
        fobj2.abstractFun(7);
    }

    private static void adderExample() {
        Adder adder = (x,y) -> x + y;

        double sum1 = adder.add(10.34, 89.11);
        System.out.println(sum1);
    }


    private static List<Person> functionAndComparatorExample() {
        Function<Long, Long> square = x -> x * x;
        Function<Long, Long> addOne = x -> x + 1;

        System.out.println(square.apply(5L));

        Function<Long, Long> squareAddOne = square.andThen(addOne);
        System.out.println(squareAddOne.apply(5L));


        Comparator<Person> lastFirstComp = Comparator.comparing(Person::getLastName)
                .thenComparing(Person::getFirstName);

        List<Person> people = new ArrayList<>();
        people.add(new Person("John", "Doe"));
        people.add(new Person("Jane", "Smith"));
        people.add(new Person("John", "Smith"));
        people.add(new Person("Alice", "Doe"));

        people.forEach(p -> System.out.println(p.getFirstName() + " " + p.getLastName()));
        System.out.println(" ");

        people.sort(lastFirstComp);

        people.forEach(p -> System.out.println(p.getFirstName() + " " + p.getLastName()));
        
        return people;
    }

    private static void streamExample() {
        List<Transaction> transactions = Arrays.asList(
                new Transaction(1, TransactionType.GROCERY, 150.0),
                new Transaction(2, TransactionType.ELECTRONICS, 200.0),
                new Transaction(3, TransactionType.GROCERY, 50.0),
                new Transaction(4, TransactionType.GROCERY, 250.0),
                new Transaction(5, TransactionType.CLOTHING, 300.0)
        );

        List<Integer> transactionsIds = transactions.stream()
                .filter(t -> t.getType() == TransactionType.GROCERY)
                .sorted(comparing(Transaction::getValue).reversed())
                .map(Transaction::getId)
                .collect(toList());

        System.out.println(transactionsIds);
    }


    private static void collectionsExample(List<Person> people) {
        List<String> list = people.stream().map(Person::getFirstName)
                .collect(Collectors.toList());

        System.out.println(list);


        List<Employee> employees = Arrays.asList(
                new Employee(1000, new Department("HR")),
                new Employee(2000, new Department("Engineer")),
                new Employee(3000, new Department("HR"))
        );

        int total = employees.stream().collect(Collectors.summingInt(Employee::getSalary));
        System.out.println(total);


        Map<Department, List<Employee>> byDept = employees.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment));
        System.out.println(byDept);


        Map<Department, Integer> totalByDept = employees.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,Collectors.summingInt(Employee::getSalary)));
    }
    
    
    public static void main(String[] args)
    {
        //functionalInterfaceExample();

//        adderExample();
//
          List<Person> people = functionAndComparatorExample();
//
//        streamExample();
//
//        collectionsExample(people);

    }

}
